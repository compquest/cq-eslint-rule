module.exports = {
    rules: {
        "no-custom-object-field-import-without-namespace": {
            meta: {
                type: "problem",
                docs: {
                    description: "add namespace while importing objects and fields",
                    category: "Possible Errors",
                    recommended: true
                }
            },
            create: context => {
                return {
                    "ImportDeclaration": node => {
                        let importStatement = node.source.value;
                        const regex = /^@salesforce\/schema\/([a-z]{1,15}__[a-zA-Z_0-9]+__c|Account|ContentDocument)(.[a-z]{1,15}__[a-zA-Z_0-9]+[__c|__r]|.Name|.RecordTypeId|.RecordType\.Name|.RecordType\.DeveloperName|.Id|.OwnerId|.CreatedDate|.CreatedById|.CreatedBy\.Name|.LastModifiedDate|.LastModifiedById|.LastModifiedBy\.Name|.CaseNumber|.Title|.Description)*$/g;
                        if(importStatement.startsWith("@salesforce/schema/") && !importStatement.match(regex)){
                            context.report({
                                node: node.source,
                                message: "Add namespace while importing custom objects or fields",
                            })
                        }
                    }
                }
            }
        }
    }
};